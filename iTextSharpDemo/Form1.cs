﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace iTextSharpDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            Document doc = new Document(PageSize.LETTER, 10, 10, 10, 10);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\archivo";
            saveFileDialog1.Title = "prueba";
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }

            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter.GetInstance(doc, file);
                doc.Open();
                //string remito = "ARCHIVO PDF DE PRUEBA";
                
                PdfPTable table = new PdfPTable(3);
                
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:/Users/desktop/Desktop/iTextSharpDemo/unelab.png");
                table.AddCell(imagen);
                
                Chunk chunk = new Chunk("Servicios", FontFactory.GetFont("ARIAL", 20f, iTextSharp.text.Font.BOLD));
                table.AddCell(getCell("Hola", PdfPCell.ALIGN_CENTER));

                table.AddCell(new Paragraph("Codigo: 1111"));

                doc.Add(table);

                doc.Add(new Paragraph("              "));

                PdfPTable table2 = new PdfPTable(2);
                PdfPTable table3 = new PdfPTable(2);
                PdfPTable table4 = new PdfPTable(1);
                PdfPTable table5 = new PdfPTable(1);
                PdfPTable table6 = new PdfPTable(1);
                PdfPTable table7 = new PdfPTable(2);
                PdfPTable table8 = new PdfPTable(1);

                table2.AddCell(new Paragraph("Cliente Empresa: Nombre Empresa", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table2.AddCell(new Paragraph("Fecha del Servicio: 09/12/2020", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table3.AddCell(new Paragraph("Fecha del Servicio: Las Condes #123, Santiago", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table3.AddCell(new Paragraph("Hora Inicio: 15:00", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table4.AddCell(new Paragraph("Hora Termino: 15:00", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));                
                table5.AddCell(new Paragraph("Trabajo Realizado: Lorem ipsum dolor sit amet consectetur adipiscing elit purus cubilia sapien, gravida a primis ridiculus ad iaculis nam donec molestie lacus, tortor litora volutpat elementum ullamcorper libero egestas turpis habitasse. Imperdiet nisl convallis netus egestas ultricies faucibus hac quisque, bibendum mauris ultrices duis fringilla torquent cras laoreet, semper velit tortor cursus turpis interdum nulla. Rhoncus morbi non donec commodo turpis pellentesque vel rutrum habitasse mauris, id purus suscipit urna congue nullam fringilla scelerisque mi tortor, lobortis ultrices taciti cursus luctus et sed praesent fusce.", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table6.AddCell(new Paragraph("Trabajo Realizado: Lorem ipsum dolor sit amet consectetur adipiscing elit purus cubilia sapien, gravida a primis ridiculus ad iaculis nam donec molestie lacus, tortor litora volutpat elementum ullamcorper libero egestas turpis habitasse. Imperdiet nisl convallis netus egestas ultricies faucibus hac quisque, bibendum mauris ultrices duis fringilla torquent cras laoreet, semper velit tortor cursus turpis interdum nulla. Rhoncus morbi non donec commodo turpis pellentesque vel rutrum habitasse mauris, id purus suscipit urna congue nullam fringilla scelerisque mi tortor, lobortis ultrices taciti cursus luctus et sed praesent fusce.", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table7.AddCell(new Paragraph("Recepcionista Servicio: Bill Gates", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                table7.AddCell(new Paragraph("Rut Recepcionista: 12.345.678-9", FontFactory.GetFont("ARIAL", 8f, iTextSharp.text.Font.NORMAL)));
                
                doc.Add(table2);
                doc.Add(table3);
                doc.Add(table4);
                doc.Add(new Paragraph("              "));
                doc.Add(table5);
                doc.Add(new Paragraph("              "));
                doc.Add(table6);
                doc.Add(new Paragraph("              "));
                doc.Add(table7);
                //GenerarDocumento(doc);
                
                doc.Add(new Paragraph("_______________________", FontFactory.GetFont("ARIAL", 12f, iTextSharp.text.Font.BOLD)));
                doc.Add(new Paragraph("Firma", FontFactory.GetFont("ARIAL", 18f, iTextSharp.text.Font.BOLD)));
                PdfPCell CellOneHdr = new PdfPCell(new Phrase("Firma"));
                table8.AddCell(CellOneHdr);
                PdfPCell CellTwoHdr = new PdfPCell(new Phrase("firma"));
                // The following property sets the cell's alignment to horizontally centered
                CellTwoHdr.HorizontalAlignment = Element.ALIGN_CENTER;
                table8.AddCell(CellTwoHdr);

                doc.Close();
                //Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }
        }
    }
}
